# README #

This README covers deployment of this project and how to use it.

### What is this repository for? ###

* An example API endpoint using Flask.
* Version 1.0

### How do I get set up? ###

* [pip install pipenv](https://pipenv.kennethreitz.org/en/latest/)
* git pull this repository.
* Run "pipenv install --deploy" in the downloaded repository location, where the Pipfile.lock is located.
* Create a .env file containing your Google Youtube API Key in the root of the project.
	* [Get your API Key](https://developers.google.com/youtube/v3/getting-started)
	* [pipenv Envrionment Variables](https://pipenv.readthedocs.io/en/latest/advanced/#automatic-loading-of-env)
* Enter the pipenv shell using "pipenv shell".
* Change directory to the webServer folder "cd ./webServer".
* Run the run.py file with "python run.py".
* The server will now be running on localhost on port 5000 "http://127.0.0.1:5000/".

### What to include in my POST request to the API endpoint. ###

This project will accept JSON POST requests to the "/API/Example" address.

The GET request headers require an "api_key" attribute, the example API key "6d606332-6b8d-45a7-b2b3-01520e02a6fc" is used in this project. 
Without this the server will return a 401 Not Authorised Response. 

You must also specify the content-type header with application/json e.g.
`{
    'api-key': '6d606332-6b8d-45a7-b2b3-01520e02a6fc',
    'content-type': 'application/json'
}`

Example JSON sent in the GET request:
`{
    'data': {
        'searchTerm': 'Tyson Fury'
    }
}`

### Using Curl ###

You can also use Curl to test the end point.

Example:

curl -d"{\"data\": {\"searchTerm\": \"Tyson Fury\"}}" -H "Content-Type: application/json" -H "api-key: 6d606332-6b8d-45a7-b2b3-01520e02a6fc"  http://127.0.0.1:5000/API/Example

No Net Example:

curl -d"{\"data\": {\"searchTerm\": \"Tyson Fury\"}}" -H "Content-Type: application/json" -H "api-key: 6d606332-6b8d-45a7-b2b3-01520e02a6fc" http://127.0.0.1:5000/API/NoNetExample

### Contact the Author ###

* Dave Fenwick dfenwick138@gmail.com