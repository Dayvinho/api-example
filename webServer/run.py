import os
import requests
import random
from flask import Flask, request, render_template, jsonify
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired
from googleapiclient.discovery import build
from json import loads


app = Flask(__name__)
SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY


class SearchForm(FlaskForm):
    searchTerm = StringField('Enter Search Term Please',
                             validators=[DataRequired()])
    submit = SubmitField('Search')


def youtubeAPI(searchTerm):
    youtubeKey = os.environ['googleYoutubeKey']
    service = build('youtube', 'v3', developerKey=youtubeKey)
    userSearch = service.search().list(q=searchTerm, part='snippet',
                                       type='video', maxResults=50,
                                       videoDuration='short')
    ytResults = userSearch.execute()
    return ytResults


def flikrAPI(searchTerm):
    flikrURL = f'''
    https://www.flickr.com/services/feeds/photos_public.gne?format=json&tags={searchTerm}
    '''  # noqa
    r = requests.get(flikrURL)
    if r.status_code == 200:
        flikrResults = dict(loads(r.text[:-1].strip('jsonFlickrFeed(')))
        return flikrResults
    else:
        return None


def keyCheck(key):
    if key == '6d606332-6b8d-45a7-b2b3-01520e02a6fc':
        return True
    else:
        return False


@app.route('/', methods=['GET', 'POST'])
@app.route('/Search', methods=['GET', 'POST'])
def searchForm():
    form = SearchForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            searchTerm = form.searchTerm.data
            youtube = youtubeAPI(searchTerm)
            flikr = flikrAPI(searchTerm)
            kwargs = {
                'form': form,
                'flikr': flikr,
                'youtube': youtube
            }
            return render_template('interview.html', **kwargs)
    kwargs = {
        'form': form
    }
    return render_template('interview.html', **kwargs)


@app.route('/API/Example', methods=['POST'])
def APIendpoint():
    if request.method == 'POST':
        try:
            key = request.headers['api-key']
        except Exception:
            response = jsonify({
                'message': 'API Endpoint for Youtube and Flikr API Services'
                })
            return response, 400
        if keyCheck(key) is True:
            query = request.get_json()
            searchTerm = query['data']['searchTerm']
            youtube = youtubeAPI(searchTerm)
            flikrTemp = flikrAPI(searchTerm)
            flikr = random.choices(flikrTemp['items'], k=3)
            flikrImgs = [img['link'] for img in flikr]
            vidID = youtube['items'][0]['id']['videoId']
            youtubeVideoURL = f'https://www.youtube.com/watch?v={vidID}'
            return jsonify(youtube=youtubeVideoURL, flikr=flikrImgs)
        else:
            response = jsonify({'message': 'Failed To Authenticate User'})
            return response, 401


@app.route('/API/NoNetExample', methods=['POST'])
def NotNetEndpoint():
    if request.method == 'POST':
        try:
            key = request.headers['api-key']
        except Exception:
            response = jsonify({
                'message': 'API Endpoint for Youtube and Flikr API Services'
                })
            return response, 400
        if keyCheck(key) is True:
            query = request.get_json()
            searchTerm = query['data']['searchTerm']
            print(searchTerm)
            payload = {
                "flikr":[
                    "https://www.flickr.com/photos/dgaproductions/48754233156/",
                    "https://www.flickr.com/photos/dgaproductions/48753912908/",
                    "https://www.flickr.com/photos/186448935@N08/49576794211/"],
                "youtube":"https://www.youtube.com/watch?v=dPmGhGOmxpI"}
            return jsonify(payload)
        else:
            response = jsonify({'message': 'Failed To Authenticate User'})
            return response, 401
if __name__ == '__main__':
    app.run(debug=False, threaded=True)
